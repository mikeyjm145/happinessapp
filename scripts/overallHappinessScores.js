happinessApp.factory('OverallHappyScore', [function () {
    return {
        averageHappiness: 0,
        minHappiness: 0,
        maxHappiness: 0,
        differenceInHappiness: 0,
        mood: ""
    };
}]);

var gatherOverallScores = function($scope, $state, happyScores) {
    var happyScore = saveHappyData.happyData;

    if (happyScore === null || happyScore.totalAttemptsAtHappiness === 0) {
        $state.go('overallHappiness.errorMessage');
        return;
    } else {
        $state.go('overallHappiness.happiness');
    }

    $scope.lastScore = happyScore.recentScore;

    happyScores.averageHappiness = happyScore.averageHappiness;
    $scope.averageHappiness = happyScores.averageHappiness;

    happyScores.minHappiness = happyScore.minHappiness;
    $scope.minHappiness = happyScores.minHappiness;

    happyScores.maxHappiness = happyScore.maxHappiness;
    $scope.maxHappiness = happyScores.maxHappiness;

    happyScores.differenceInHappiness = differenceInHappiness(happyScore.recentScore, happyScore.averageHappiness);
    $scope.differenceInHappiness = happyScores.differenceInHappiness;

    happyScores.mood = setMood(happyScore.recentScore);
    $scope.mood = happyScores.mood;

};

happinessApp.controller('OverallHappiness', [
    '$scope',
    '$state',
    'OverallHappyScore',
    gatherOverallScores
]);