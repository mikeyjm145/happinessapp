happinessApp.factory('reasons', [function() {
	return [
        {
            id: 'Reason #1',
            description: "Increase happiness"
        },
        {
            id: 'Reason #2',
            description: "Decrease unhappiness"
        },
        {
            id: 'Reason #3',
            description: "Because we care"
        },
		{
			id: 'Reason #4',
			description: "Increase your neighbor's current happiness"
		},
		{
			id: 'Reason #5',
			description: "The world likes it when you are happy"
		}
    ];
}]);

happinessApp.controller('ReasonsController', 
	[
		'$scope',
		'reasons',
		listReasons
	]
);