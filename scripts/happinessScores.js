﻿happinessApp.factory('HappyScore', [function () {
    return {
        recentScore: 0,
        totalScore: 0,
        totalAttemptsAtHappiness: 0,
        averageHappiness: 0,
        minHappiness: 0,
        maxHappiness: 0
    };
}]);

var accumulateScore = function ($scope, $state, happyScore) {
    //console.log(JSON.parse($stateParams.works));
    if (saveHappyData.happyData !== null) {
        happyScore = saveHappyData.happyData;
    } else {
        saveHappyData.happyData = happyScore;
    }

    $scope.recentScore = {value: 0};

    $scope.importHappiness = function() {
        happyScore = saveHappyData.happyData;

        $scope.lastScore = happyScore.recentScore;
        $scope.totalAttemptsAtHappiness = happyScore.totalAttemptsAtHappiness;
        $scope.averageHappiness = happyScore.averageHappiness;
    };

    $scope.setScores = function () {
        var lastScore = $scope.recentScore.value;

        if (lastScore === 0) {
            $scope.message = 'Please enter a number between 1-10';
            return;
        }

        happyScore.totalAttemptsAtHappiness = increment(happyScore.totalAttemptsAtHappiness);

        happyScore.recentScore = lastScore;

        happyScore.totalScore = sum(happyScore.totalScore, happyScore.recentScore);

        happyScore.averageHappiness = average(happyScore.totalScore, happyScore.totalAttemptsAtHappiness);

        happyScore.minHappiness = setMin(happyScore.minHappiness, lastScore);

        happyScore.maxHappiness = setMax(happyScore.maxHappiness, lastScore);

        toggleElementById("submit", true);

        $scope.message = 'Check out your overall happiness rating!';

        saveHappyData.happyData = happyScore;

        $state.go('happiness.averageHappiness');
    };
};

happinessApp.controller('HappyScoreController', [
    '$scope',
    '$state',
    'HappyScore',
    accumulateScore
]);