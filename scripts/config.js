happinessApp.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider 
        
        // home state with nested views
        .state('home', {
            url: '/home',
            templateUrl: 'html/home.html'
        })
        
        // nested view with anonymous controller
        .state('home.why', {
            url: '/why',
            templateUrl: 'html/home-why.html',
            controller: function($scope) {
                $scope.reasons = ['Happiness makes you happy.', 
							   'Unhappiness makes you sad.', 
							   'Tracking happiness can make you happy.',
							   'Increase your neighbor"s current happiness',
							   'The world likes it when you are happy'];
            }
        })
        
        // nested view with an in-line template and no controller
        .state('home.what', {
            url: '/what',
            template: 'This app lets you track how happy you are at the moment.',
			controller: function($scope) {
                $scope.reasons = ['Happiness makes you happy.', 
							   'Unhappiness makes you sad.', 
							   'Tracking happiness can make you happy.',
							   'Increase your neighbor"s current happiness',
							   'The world likes it when you are happy'];
            }
        })
		
		.state('home.reason', {
            url: '/reason',
            views: {
                '': { templateUrl: 'html/about.html' },
                'columnOne@about': { template: 'We are The Happiness People&#0153;.' },
                'columnTwo@about': { 
                    templateUrl: 'html/reasons-data.html',
                    controller: 'ReasonsController'
                }
            }
        })
        
        // about state with named views
        .state('about', {
            url: '/about',
            views: {
                '': { templateUrl: 'html/about.html' },
                'columnOne@about': { template: 'We are The Happiness People&#0153;.' },
                'columnTwo@about': { 
                    templateUrl: 'html/reasons-data.html',
                    controller: 'ReasonsController'
                }
            }
            
        })
        
        .state('happiness', {
            url: '/happiness',
            controller: 'HappyScoreController',
            templateUrl: 'html/happiness.html'
        })

        .state('happiness.rateHappiness', {
            views: {
                '': { templateUrl: 'html/rateHappiness.html' }
            }
        })
        
        .state('happiness.averageHappiness', {
            views: {
                '': { templateUrl: 'html/averageHappiness.html' }
            }
        })

        .state('overallHappiness', {
            url: '/overall_happiness',
            controller: 'OverallHappiness',
            templateUrl: 'html/overallHappiness.html'
        })

        .state('overallHappiness.happiness', {
            views: {
                '': { templateUrl: 'html/overallHappinessLevel.html' }
            }
        })

        .state('overallHappiness.errorMessage', {
            views: {
                '': { templateUrl: 'html/overallHappinessErrorMessage.html' }
            }
        });
        
});