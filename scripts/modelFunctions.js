﻿var sum = function(total, value) {
    return total + value;
};

var average = function(total, times) {
    return total / times;
};

var setMax = function(prevMax, newMax) {
    if (prevMax === 0) {
        return newMax;
    }

    return Math.max(prevMax, newMax);
};

var setMin = function(prevMin, newMin) {
    if (prevMin === 0) {
        return newMin;
    }

    return Math.min(prevMin, newMin);
};

var differenceInHappiness = function(recent, average) {
    return recent - average;
};

var increment = function(amount) {
    return amount + 1;
};

var setMood = function(amount) {
    var mood = "";

    if (amount <= 2) {
        mood = "even-tempered"
    } else if (amount >= 6) {
        mood = "moody";
    } else {
        mood = "in-between";
    }

    return mood;
};

var saveHappyData = {
    happyData: null
};